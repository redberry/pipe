package cc.redberry.pipe;

public class PInterruptedException extends RuntimeException {
    public PInterruptedException(Throwable cause) {
        super(cause);
    }

    public PInterruptedException(String message, Throwable cause) {
        super(message, cause);

    }

    public PInterruptedException(String message) {

        super(message);
    }

    public PInterruptedException() {

    }
}
