/*
 * cc.redberry.pipe: java library for implementation of concurrent pipelines
 * Copyright (c) 2010-2012
 * Bolotin Dmitriy <bolotin.dmitriy@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cc.redberry.pipe.util;

/**
 * Interface to handle exceptions thrown by {@link cc.redberry.pipe.OutputPort#take()}, {@link
 * cc.redberry.pipe.Processor#process(Object)} and {@link cc.redberry.pipe.VoidProcessor#process(Object)}
 * methods in threads created by framework infrastructure..
 */
public interface UncaughtExceptionHandler<S> {
    /**
     * Executed from execution threads.
     *
     * @param exception thrown exception
     */
    public void handle(RuntimeException exception, S source);
}
