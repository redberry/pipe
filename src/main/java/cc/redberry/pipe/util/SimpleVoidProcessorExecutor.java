/*
 * cc.redberry.pipe: java library for implementation of concurrent pipelines
 * Copyright (c) 2010-2012
 * Bolotin Dmitriy <bolotin.dmitriy@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cc.redberry.pipe.util;

import cc.redberry.pipe.OutputPort;
import cc.redberry.pipe.VoidProcessor;

/**
 * @param <T>
 * @author Bolotin Dmitriy (bolotin.dmitriy@gmail.com)
 */
public class SimpleVoidProcessorExecutor<T> implements Runnable {
    private final OutputPort<T> input;
    private final VoidProcessor<? super T> processor;

    public SimpleVoidProcessorExecutor(OutputPort<T> input, VoidProcessor<? super T> processor) {
        this.input = input;
        this.processor = processor;
    }

    @Override
    public void run() {
        T object;
        while ((object = input.take()) != null)
            processor.process(object);
    }
}
