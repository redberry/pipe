/*
 * cc.redberry.pipe: java library for implementation of concurrent pipelines
 * Copyright (c) 2010-2012
 * Bolotin Dmitriy <bolotin.dmitriy@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cc.redberry.pipe.util;

import cc.redberry.pipe.OutputPort;
import cc.redberry.pipe.OutputPortCloseable;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class CountLimitingOutputPort<T> implements OutputPortCloseable<T> {
    private final OutputPort<T> innerPort;
    private final long limit;
    private final AtomicLong count;
    private final AtomicBoolean finished = new AtomicBoolean(false);

    public CountLimitingOutputPort(OutputPort<T> innerPort, long limit) {
        if (innerPort == null)
            throw new NullPointerException();
        this.innerPort = innerPort;
        this.limit = limit;
        this.count = new AtomicLong(-limit);
    }

    public long getLimit() {
        return limit;
    }

    public long getElementsLeft() {
        long left = count.get();
        if (left > 0)
            return 0;
        return -left;
    }

    public boolean isClosed() {
        return finished.get();
    }

    @Override
    public T take() {
        if (finished.get())
            return null;
        long index = count.incrementAndGet();
        if (index > 0) {
            if (finished.compareAndSet(false, true))
                if (innerPort instanceof OutputPortCloseable)
                    ((OutputPortCloseable) innerPort).close();
            return null;
        }
        T value = innerPort.take();
        if (value == null)
            finished.set(true);
        return value;
    }

    @Override
    public void close() {
        finished.set(true);
        if (innerPort instanceof OutputPortCloseable)
            ((OutputPortCloseable) innerPort).close();
    }
}
